const { app, BrowserWindow, ipcMain, dialog } = require('electron')

//复用创建窗口得方法
class AppWindow extends BrowserWindow {
  constructor(config, fileLocation) {
    const basicConfig = {
      width: 800,
      height: 600,
      webPreferences: {
        nodeIntegration: true
      }
    }
    //这段代码啥意思
    const finalConfig = { ...basicConfig, ...config }
    super(finalConfig)
    this.loadFile(fileLocation)
    this.once('ready-to-show', () => {
      this.show()
    })
  }
}
app.on('ready', () => {
  const mainWindow = new AppWindow({}, './renderer/index.html')
  ipcMain.on('add-music-window', () => {
    console.log('333333')
    const addWindow = new AppWindow({
      width: 500,
      height: 400,
      parent: mainWindow
    }, './renderer/add.html')
  })
  mainWindow.webContents.openDevTools();

  ipcMain.on('open-music-file', (event,arg) => {
    console.log('sdfsdgdfhg33333')
    const files=  dialog.showOpenDialog({
      properties: ['openFile', 'multiSelections'],
      filters: [{ name: 'Music', extensions: ['mp3'] }]
    })
    console.log(files);
    if (files) {
      console.log('=======');
      event.sender.send('selected-file', files)
    }
    console.log('88888');
  })

})

